Author: Dillon Flohr <DillonFlohr@yahoo.com>

Submission for the Banno Intership Programming Assignment.

To Run: (Please contact me if this doesn't run on your machine.)

Please clone this repo onto a 64-bit linux machine.
Go into the bannoproject folder in your terminal and run "./main"

The output should be some text about getting the HTML then:
---------------------------------------------------------------------------
HTML GET of banno.com completed.


The most frequent alphanumeric character is 'e' with 1199 occurrences.
The second most frequent alphanumeric character is 't' with 1007 occurrences.
The third most frequent alphanumeric character is 'o' with 992 occurrences.

The number of PNGs in the HTML is 7.

The twitter handle of Banno is @bannoJHA.

The amount of occurrences of "financial institution" in text is 2.

-------------------------------------------------------------------------------

Thank you for taking the time to review my code.

(Note: You will not be able to compile the code unless you have libcurl installed onto you machine. https://curl.haxx.se/download.html)