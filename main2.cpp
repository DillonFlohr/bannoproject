//c includes
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <bits/stdc++.h>
//c++ includes
#include <iostream>
#include <fstream>
#include <string>
//my file includes
#include "getAndWriteHTML.c"
#include "countCharacters.cpp"
#include "countPNGs.cpp"
#include "getTwitter.cpp"
#include "countText.cpp"

using namespace std;

int main(int argc, char *argv[]) {
	//Get the HTML and put it in page.out
  	getAndWriteHTML();
  	//Report that the HTML has been GETen.
	std::cout << endl << "HTML GET of banno.com completed." << endl << endl << endl;
	//Count the alphanumeric characters and report the top 3
	countCharacters();
	//Count the PNGs
	countPNGs();
	//Get Banno's Twitter handle
	getTwitter();
	//Count occurances of financial institution in text
	countText();
	return 0;
}
