using namespace std;
int countCharacters() {
	//dictionary to hold the characters
	unordered_map<char, int> alphanumericMap;
	//open the file
	ifstream is("page.out");
	string str;
	//list to check if a char is alphanumeric
	string alphanumerics{"AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890"};
	//for each line
	while(getline(is, str)) {
		//for each character
		for (char c : str) {
			//if c is an alphanumeric character
			std::size_t found = alphanumerics.find(c);
			if (found!=std::string::npos) {
				//if the character has not allready been found add it to the map
				if (alphanumericMap.find(c) == alphanumericMap.end()) {
				alphanumericMap[c] = 1;
				}
				// else increment its count by one
				else {
					alphanumericMap[c] = alphanumericMap[c] + 1;
				}
			}
		}
	}
	//max starts at zero and the maxc(haracter) starts as a space
	int max{0};
	char maxc1{' '};
	int max2{0};
	char maxc2{' '};
	int max3{0};
	char maxc3{' '};
	int size{alphanumericMap.size()};
	//for all the keys in the map
	for (int e = 0; e <= size; ++e) {
		//get the letter we are checking
		char c = alphanumerics[e];
		//if it is more than any other checked make it the max
		if (alphanumericMap[c] > max) {
  			max = alphanumericMap[c];
  			maxc1 = c;
		}
	}
	for (int e = 0; e <= size; ++e) {
		char c = alphanumerics[e];
		//if it more than any checked and it is not the same as the first place character
		if (alphanumericMap[c] > max2 and alphanumericMap[c] != max) {
			max2 = alphanumericMap[c];
			maxc2 = c;
		}
	}
	for (int e = 0; e <= size; ++e) {
		char c = alphanumerics[e];
		//if it not the same as the first and second place characters
		if (alphanumericMap[c] > max3 and alphanumericMap[c] != max and alphanumericMap[c] != max2) {
			max3 = alphanumericMap[c];
			maxc3 = c;
		}
	}
	is.close();
	cout << "The most frequent alphanumeric character is '" << maxc1 << "' with " << max << " occurrences." << 	endl;
	cout << "The second most frequent alphanumeric character is '" << maxc2 << "' with " << max2 << " occurrences." << endl;
	cout << "The third most frequent alphanumeric character is '" << maxc3 << "' with " << max3 << " occurrences." << endl;
	cout << endl;
	return 0;
}
