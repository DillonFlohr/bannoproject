using namespace std;
int countText() {
	ifstream is("page.out");
  	string str;
	int result{0};
	int size{11};
	string tagList[] {"<h1","<p","<b","<strong","<i","<em","<mark","<small","<del","<ins","<sub","<sup"};
	//for each line
  	while(getline(is, str)) {
		//for each text producing tag
		for (int e = 0; e <= size; ++e) {
			//get what we are checking
			string s = tagList[e];
			//see if its in the line
			std::size_t found2 = str.find(s);
			//and see if it has "financial institution"
			std::size_t found3 = str.find("financial institution");
			if (found2!=std::string::npos and found3!=std::string::npos) {
				result += 1;
			}
		}
	}
	is.close();
	cout << "The amount of occurrences of \"financial institution\" in text is " << result << "." << endl << endl;
	return 0;
}