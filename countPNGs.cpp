using namespace std;
int countPNGs() {
	ifstream is("page.out");
  	string str;
	int count{0};
	//for each line
  	while(getline(is, str)) {
		//if a ling is an <img> tag
		std::size_t found = str.find("<img");
  		if (found!=std::string::npos) {
			//add to the count of the img total
			count += 1;
		}
	}
	is.close();
	cout << "The number of PNGs in the HTML is " << count << "." << endl << endl;
	return 0;
}